﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Test.Models;

namespace Test.Helpers
{
    public static class ReportHelper
    {
        public static string GetFolderRoot() => Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName).FullName;
        public static string SetFilePath(string fileName) => string.Format("{0}//Data//{1}.csv", GetFolderRoot(), fileName);
        public static bool Export(string filePath, List<Phones> lst)
        {
            try
            {
                FileStream fs = new FileStream(filePath, FileMode.Create);//Tạo file mới theo đường dẫn           
                using (StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8))//fs là 1 FileStream
                {
                    sWriter.WriteLine("PHONE_NUMBER,REAL_ACTIVATION_DATE");
                    foreach (var item in lst)
                    {
                        sWriter.WriteLine(item.PhoneNumber + "," + item.DateActive.ToString("yyyy-MM-dd"));
                    }
                    sWriter.Flush();
                }
                fs.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public static List<Phones> Import(string filePath)
        {
            var a = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName).FullName;
            // Tên file rỗng hoặc đường dẫn file không tồn tại 
            if (string.IsNullOrEmpty(filePath) || !File.Exists(filePath))
            {
                return null;
            }
            try
            {
                List<Phones> list = new List<Phones>();
                StreamReader sr = new StreamReader(filePath);
                string strline = "";
                int x = 0;
                while (!sr.EndOfStream)
                {
                    x++;
                    strline = sr.ReadLine();
                    if (x == 1)
                    {
                        continue;
                    }
                    if (!string.IsNullOrEmpty(strline) && strline.Contains(","))
                    {
                        var arr = strline.Split(",");
                        list.Add(new Phones(arr[0], Convert.ToDateTime(arr[1]), string.IsNullOrEmpty(arr[2]) ? (DateTime?)null : Convert.ToDateTime(arr[2])));
                    }
                }
                sr.Close();
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
