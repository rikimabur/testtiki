﻿using System;

namespace Test.Models
{
    public class Phones
    {
        public string PhoneNumber { get; set; }
        public DateTime DateActive { get; set; }
        public DateTime? DateDeActive { get; set; }
        public Phones(string phoneNumber, DateTime dateActive, DateTime? dateDeActive)
        {
            this.PhoneNumber = phoneNumber;
            this.DateActive = dateActive;
            this.DateDeActive = dateDeActive;
        }
    }
}
