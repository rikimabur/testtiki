﻿ using System;
using Test.Helpers;
using Test.Services;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            PhoneServices phoneService = new PhoneServices();
            var lstImport = ReportHelper.Import(ReportHelper.SetFilePath("input"));
            if (lstImport != null)
            {
                var lstExport = phoneService.Process(lstImport);
                // Danh sách có ít nhất 1 phần tử
                if (lstExport != null)
                {
                    if (ReportHelper.Export(ReportHelper.SetFilePath("output"), lstExport))
                    {
                        Console.WriteLine("Written file .csv success");
                    }
                    else
                    {
                        Console.WriteLine("Written file .csv fail");
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
