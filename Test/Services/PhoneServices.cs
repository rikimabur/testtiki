﻿using System;
using System.Collections.Generic;
using System.Linq;
using Test.Models;

namespace Test.Services
{
    public class PhoneServices
    {
        public List<Phones> Process(List<Phones> phones)
        {
            try
            {
                // Danh sách thuê bao export 
                List<Phones> lstPhoneEx = new List<Phones>();
                for (int i = 0; i < phones.Count; i++)
                {
                    var item = phones[i];
                    // Số điện này đã tồn tại trong danh sách export
                    if (lstPhoneEx != null && lstPhoneEx.Any(x => x.PhoneNumber == item.PhoneNumber))
                    {
                        continue;
                    }
                    // Tìm kiếm số điện thoại dựa vào item và nếu danh sách có phần tử thì sắp xếp tăng dần theo DetaActivation
                    List<Phones> list = phones.FindAll(x => x.PhoneNumber == item.PhoneNumber)?.OrderBy(x => x.DateActive).ToList();
                    //Kiêm tra mảng khác rỗng và danh sách phải ít nhất từ 2 phần tử
                    if (list != null && list.Count > 1)
                    {
                        lstPhoneEx.Add(FindActivationDate(list));
                    }
                    else
                    {
                        lstPhoneEx.Add(item);
                    }
                }
                return lstPhoneEx;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        /// <summary>
        /// Tìm ngày kích hoạt thực tế dựa trên danh sách
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        private Phones FindActivationDate(List<Phones> list)
        {
            Phones phonesNews;
            phonesNews = list[0];
            for (int i = list.Count - 1; i > 0; i--)
            {
                DateTime dateCurrent = list[i].DateActive;
                DateTime? dateNext = list[i - 1].DateDeActive;
                // Nếu DateDeActive != nullable và tháng >=1 chuyển thành chủ thuê bao mới
                if (dateNext.HasValue && ValidateDate(dateNext, dateCurrent))
                {
                    return list[i];
                }
            }
            return phonesNews;
        }
        /// <summary>
        /// Kiểm tra khoảng cách giữa 2 thời điểm có >= 1 tháng hay không
        /// </summary>
        /// <param name="dateTime1"></param>
        /// <param name="dateTime2"></param>
        /// <returns>bool</returns>
        private bool ValidateDate(DateTime? dateTime1, DateTime dateTime2)
        {
            return (dateTime2 - dateTime1.Value).TotalDays >= 30;
        }
    }
}
